package com.optime.switchkit.Model;

/**
 * Created by rmadan on 7/12/2015.
 */
public class TCPScoketResponse {
    String res;

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }
}
