package com.optime.switchkit.Model;

/**
 * Created by rmadan on 7/14/2015.
 */
public class MacResponse {
    String mac;
    boolean configured;

    public boolean isConfigured() {
        return configured;
    }

    public void setConfigured(boolean configured) {
        this.configured = configured;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
