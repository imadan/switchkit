package com.optime.switchkit.Model;

/**
 * Created by rmadan on 7/24/2015.
 */
public class GenerateOTPResponse {
    int otp;
    String user_id;

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
