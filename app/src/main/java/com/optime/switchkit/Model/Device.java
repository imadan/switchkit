package com.optime.switchkit.Model;

/**
 * Created by rmadan on 7/5/2015.
 */
public class Device {
    String device_id;
    String device_name;
    int sharedUsers;
    boolean last_state;
    int color;
    int brightness;
   // String shared_with;
    String wifi_name;
    String wifi_password;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public int getSharedUsers() {
        return sharedUsers;
    }

    public void setSharedUsers(int sharedUsers) {
        this.sharedUsers = sharedUsers;
    }

    public boolean isLast_state() {
        return last_state;
    }

    public void setLast_state(boolean last_state) {
        this.last_state = last_state;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getBrightness() {
        return brightness;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public String getWifi_name() {
        return wifi_name;
    }

    public void setWifi_name(String wifi_name) {
        this.wifi_name = wifi_name;
    }

    public String getWifi_password() {
        return wifi_password;
    }

    public void setWifi_password(String wifi_password) {
        this.wifi_password = wifi_password;
    }
}
