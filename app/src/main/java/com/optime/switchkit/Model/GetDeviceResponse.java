package com.optime.switchkit.Model;

import java.util.ArrayList;

/**
 * Created by rmadan on 7/18/2015.
 */
public class GetDeviceResponse {

    ArrayList<Device> devices ;

    public ArrayList<Device> getDevices() {
        return devices;
    }

    public void setDevices(ArrayList<Device> devices) {
        this.devices = devices;
    }
}
