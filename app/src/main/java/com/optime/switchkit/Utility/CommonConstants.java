package com.optime.switchkit.Utility;

/**
 * Created by rmadan on 1/14/2015.
 */
public class CommonConstants {

    public static String SERVER_URL = "http://ec2-52-74-105-217.ap-southeast-1.compute.amazonaws.com:8000";

    public static String USER_SETTINGS_PREFERENCE = "user_settings_preference";
    public static String USER_ID = "user_id";
    public static String USER_MOBILE = "user_mobile";
    public static String USER_FNAME = "user_fname";
    public static String USER_LNAME = "user_lname";
    public static String USER_TOKEN = "user_token";
    public static String LOGIN_PROVIDER = "login_provider";
    public static String USER_TOKEN_EXPIRY = "user_token_expiry";
    public static String USER_EMAIL = "user_email";
    public static String USER_PWD = "user_pwd";
    public static String USER_FBID = "user_fbid";
    public static String USER_GOOGLEID = "user_googleid";
    public static String USER_TERMS_ACCEPTANCE = "terms_acceptance";
    public static String USER_FIRST_LAUNCH = "first_launch";
    public static String USER_ACCESS_TOKEN = "access_token";
    public static String MEDIA_PLAYER = "mp";
    public static String TUTONE = "tut_one";
    public static String TUTTWO = "tut_two";
    public static String TUTTHREE = "tut_three";
    public static String TUTFOUR = "tut_four";
    public static String TUTFIVE = "tut_five";
    public static String CURR_BALANCE = "curr_balance";
}
