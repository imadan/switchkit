package com.optime.switchkit.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Display;
import android.view.WindowManager;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by rmadan on 5/29/2015.
 */
public class Utility {

    public static String[] monthHash1 = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"};


    public static double dp = 2;
    public static int screenHeight = 1080;
    public static int screenWidth = 720;

    public static int xxhdpiHeight = 1920;
    public static int xxhdpiWidth = 1080;

    public static int xxxhdpiHeight = 2560;
    public static int xxxhdpiWidth = 1440;

    public static int benchmarkWidth = xxhdpiWidth;
    public static int benchmarkHeight = xxhdpiHeight;

    public static String BASE_DIR =  Environment.getExternalStorageDirectory().getAbsolutePath();
    public static String MII_DIR = BASE_DIR ;//+ "/mii";
    public static String IMAGE_DIR = MII_DIR + "/MiiImages/";
    public static String AUDIO_DIR = MII_DIR + "/VTRadio/";
    public static String VIDEO_DIR = MII_DIR + "/MiiVideos/";
    public static String FILE_DIR = MII_DIR + "/MiiFiles/";

    private static HashMap<String, String> monthHash;


    public static String ConvertTime(String date)
    {
        if (date != null) {
            String onlyDate = "";
            if (date.split("T").length > 1) {
                onlyDate = date.split("T")[1];
            } else if(date.split(" ").length > 1) {
                onlyDate = date.split(" ")[1];
            }
            int hourNum = Integer.valueOf(onlyDate.split(":")[0]);
            int min = Integer.valueOf(onlyDate.split(":")[1]);
            // String monthChar = new DateFormatSymbols().getMonths()[month-1];
            String h, m;
            if (hourNum < 10) {
                h = "0" + hourNum;
            } else {
                h = hourNum + "";
            }
            if (min < 10) {
                m = "0" + min;
            } else {
                m = min + "";
            }
            return h + ":" + m;
            /*if (hourNum > 12) {
                hourNum = hourNum - 12;
                if (min < 10) {
                    return hourNum + ":0" + min + " pm";
                } else {
                    return hourNum + ":" + min + " pm";
                }
            } else {
                if (min < 10) {
                    return hourNum + ":0" + min + " am";
                } else {
                    return hourNum + ":" + min + " am";
                }
            }
            */
        }
        return "";

    }

    public static int GetCurrentDay(int offset)
    {
        Date date = new Date(); // your date
        //date = date;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, offset);
        // dt = c.getTime();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_WEEK);
        return day;

    }


    public static String GetDisplayDate(int offset)
    {
        Date date = new Date(); // your date
        //date = date;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, offset);
        // dt = c.getTime();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DATE);
        return day + " " + monthHash1[month];

    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean ShowNetworkDialog(final Context context)
    {

        if (isNetworkAvailable(context)) {
            return true;
        }


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("There seems to be no internet connectivity. Please check your internet connection and try again");
        alertDialogBuilder.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        //   context.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        context.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));

                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        try {
            alertDialog.show();
        } catch (Exception e) {

        }

        return false;
    }

    public static void GetScreenResolution(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        screenWidth = width;
        screenHeight = height;
        if (width < 350) {
            dp = 1;
            // Toast.makeText(context, " mdpi device",
            //         Toast.LENGTH_LONG).show();
        } else if (width >350 && width<510) {
            dp = 1.5;
            // Toast.makeText(context, " hdpi device",
            //         Toast.LENGTH_LONG).show();
        } else if (width > 510 && width < 820) {
            dp = 2;
            // Toast.makeText(context, " xhdpi device",
            //         Toast.LENGTH_LONG).show();
        } else if (width > 510 && width < 820) {
            dp = 3;
            // Toast.makeText(context, " xxhdpi device",
            //         Toast.LENGTH_LONG).show();
        } else {
            dp = 4;
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentUri, int mediaType) {
        Cursor cursor = null;
        int column_index;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            if (mediaType == 1) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            } else if (mediaType == 2) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            } else if (mediaType == 3) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);
            } else {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            }
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    public static String ConvertTime1(String date)
    {
        if (date != null) {
            String onlyDate = "";
            if (date.split("T").length > 1) {
                onlyDate = date.split("T")[1];
            } else if(date.split(" ").length > 1) {
                onlyDate = date.split(" ")[1];
            }
            int hourNum = Integer.valueOf(onlyDate.split(":")[0]);
            int min = Integer.valueOf(onlyDate.split(":")[1]);
            // String monthChar = new DateFormatSymbols().getMonths()[month-1];
            if (hourNum > 12) {
                hourNum = hourNum - 12;
                if (min < 10) {
                    return hourNum + ":0" + min + " pm";
                } else {
                    return hourNum + ":" + min + " pm";
                }
            } else {
                if (min < 10) {
                    return hourNum + ":0" + min + " am";
                } else {
                    return hourNum + ":" + min + " am";
                }
            }
        }
        return "";

    }


    public static String ConvertDate(String date)
    {

        if (monthHash != null) {

        } else {

            monthHash = new HashMap<String, String>();
            monthHash.put("January", "Jan");
            monthHash.put("February", "Feb");
            monthHash.put("March", "Mar");
            monthHash.put("April", "Apr");
            monthHash.put("May", "May");
            monthHash.put("June", "Jun");
            monthHash.put("July", "July");
            monthHash.put("August", "Aug");
            monthHash.put("September", "Sept");
            monthHash.put("October", "Oct");
            monthHash.put("November", "Nov");
            monthHash.put("December", "Dec");

        }
        if (date != null) {
            String onlyDate = "";
            if (date.split("T").length > 1) {
                onlyDate = date.split("T")[0];
            } else if(date.split(" ").length > 1) {
                onlyDate = date.split(" ")[0];
            }
            int dateNum = Integer.valueOf(onlyDate.split("-")[2]);
            int month = Integer.valueOf(onlyDate.split("-")[1]);
            String monthChar = new DateFormatSymbols().getMonths()[month - 1];
            return dateNum + " " + monthHash.get(monthChar);
        }
        return "";
    }

    public static String getPath(Uri uri, Activity activity) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static int GenerateRandomNumber()
    {

        int max = 1000000;
        int min = 2;
        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }


}
