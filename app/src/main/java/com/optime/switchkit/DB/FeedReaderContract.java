package com.optime.switchkit.DB;

import android.provider.BaseColumns;

/**
 * Created by rmadan on 7/5/2015.
 */
public final class FeedReaderContract {

    public static abstract class Device implements BaseColumns {
        public static final String TABLE_NAME = "device";
        public static final String COLUMN_NAME_DID = "did";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_SHARED_USERS = "shared_users";
        public static final String COLUMN_NAME_COLOR = "color";
        public static final String COLUMN_NAME_BRIGHTNESS = "brightness";
        public static final String COLUMN_NAME_STATE = "state";
        public static final String COLUMN_NAME_SHARED_NAMES = "shared_names";
        public static final String COLUMN_NAME_SSID = "wifi_ssid";
        public static final String COLUMN_NAME_WIFI_PWD = "wifi_pwd";
    }
}
