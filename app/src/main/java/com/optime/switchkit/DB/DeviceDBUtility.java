package com.optime.switchkit.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.optime.switchkit.Model.Device;

import java.util.ArrayList;

/**
 * Created by rmadan on 7/5/2015.
 */
public class DeviceDBUtility {

    public static DeviceDBHelper deviceDBHelper;

    public DeviceDBHelper CreateDeviceDB(Context context)
    {
        if (deviceDBHelper == null) {
            deviceDBHelper = new DeviceDBHelper(context);
        }

        return deviceDBHelper;

    }

    public long AddStatusToDB(DeviceDBHelper deviceDBHelper, Device status)
    {
        // Gets the data repository in write mode
        SQLiteDatabase db = deviceDBHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();


        values.put(FeedReaderContract.Device.COLUMN_NAME_DID, status.getDevice_id());
        values.put(FeedReaderContract.Device.COLUMN_NAME_NAME, status.getDevice_name());
        values.put(FeedReaderContract.Device.COLUMN_NAME_STATE, status.isLast_state());
        values.put(FeedReaderContract.Device.COLUMN_NAME_SHARED_USERS, status.getSharedUsers());
        values.put(FeedReaderContract.Device.COLUMN_NAME_BRIGHTNESS, status.getBrightness());
        values.put(FeedReaderContract.Device.COLUMN_NAME_COLOR, status.getColor());
     //   values.put(FeedReaderContract.Device.COLUMN_NAME_SHARED_NAMES, status.getShared_with());
        values.put(FeedReaderContract.Device.COLUMN_NAME_WIFI_PWD, status.getWifi_password());
        values.put(FeedReaderContract.Device.COLUMN_NAME_SSID, status.getWifi_name());


        // Insert the new row, returning the primary key value of the new row
        long newRowId;
        newRowId = db.insert(
                FeedReaderContract.Device.TABLE_NAME,
                null,
                values);
        return newRowId;
    }


    public void DeleteFromDB(DeviceDBHelper deviceDBHelper, String uid)
    {
        SQLiteDatabase db = deviceDBHelper.getReadableDatabase();

        db.execSQL("delete from " + FeedReaderContract.Device.TABLE_NAME + " where uid = " + uid  );
        //db.execSQL("update " + FeedReaderContract.UserChats.TABLE_NAME + " set state = 4 where mid <= " + lastRead );
    }

    public void UpdateDeviceState(DeviceDBHelper deviceDBHelper, String did, boolean state)
    {
        SQLiteDatabase db = deviceDBHelper.getReadableDatabase();

        if (state) {
            db.execSQL("update " + FeedReaderContract.Device.TABLE_NAME + " set " + FeedReaderContract.Device.COLUMN_NAME_STATE + " = " + 1 + " where did = '" + did + "'");
        } else {
            db.execSQL("update " + FeedReaderContract.Device.TABLE_NAME + " set " + FeedReaderContract.Device.COLUMN_NAME_STATE + " = " + 0 + " where did like '" + did  + "'" );
        }
    }

    public void UpdateDeviceBrightness(DeviceDBHelper deviceDBHelper, String did, int brightness)
    {
        SQLiteDatabase db = deviceDBHelper.getReadableDatabase();

        db.execSQL("update " + FeedReaderContract.Device.TABLE_NAME + " set " + FeedReaderContract.Device.COLUMN_NAME_BRIGHTNESS + " = " + brightness + " where did like '" + did + "'" );
    }


    public void UpdateDeviceColor(DeviceDBHelper deviceDBHelper, String did, int color)
    {
        SQLiteDatabase db = deviceDBHelper.getReadableDatabase();

        db.execSQL("update " + FeedReaderContract.Device.TABLE_NAME + " set " + FeedReaderContract.Device.COLUMN_NAME_COLOR + " = " + color + " where did = " + did  );
    }


    public void UpdateDeviceName(DeviceDBHelper deviceDBHelper, String did, String name)
    {
        SQLiteDatabase db = deviceDBHelper.getReadableDatabase();

        db.execSQL("update " + FeedReaderContract.Device.TABLE_NAME + " set " + FeedReaderContract.Device.COLUMN_NAME_NAME + " = '" + name + "' where did = " + did  );
    }

    Cursor GetRowsFromChatListDB(DeviceDBHelper deviceDBHelper, String did)
    {
        SQLiteDatabase db = deviceDBHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                FeedReaderContract.Device.COLUMN_NAME_DID,
                FeedReaderContract.Device.COLUMN_NAME_NAME,
                FeedReaderContract.Device.COLUMN_NAME_STATE,
                FeedReaderContract.Device.COLUMN_NAME_BRIGHTNESS,
                FeedReaderContract.Device.COLUMN_NAME_COLOR,
                FeedReaderContract.Device.COLUMN_NAME_SHARED_USERS,
                FeedReaderContract.Device.COLUMN_NAME_SHARED_NAMES,
                FeedReaderContract.Device.COLUMN_NAME_WIFI_PWD,
                FeedReaderContract.Device.COLUMN_NAME_SSID,

        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                FeedReaderContract.Device.COLUMN_NAME_DID + " DESC ";
        String whereClause = "" ;
        if (!did.equals("")) {
            whereClause = FeedReaderContract.Device.COLUMN_NAME_DID + " like '" + did + "'";
        }

        Cursor c = db.query(
                FeedReaderContract.Device.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                whereClause,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
        return c;
    }

    public ArrayList<Device> GetDevice(DeviceDBHelper deviceDBHelper, String did)
    {
        Cursor cursor = GetRowsFromChatListDB(deviceDBHelper, did);
        ArrayList<Device> devices = new ArrayList<Device>();
        Device device;

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false)
        {
            device = new Device();
            device.setDevice_id(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_DID)));
            device.setDevice_name(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_NAME)));
            if (cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_STATE)) == 0) {
                device.setLast_state(false);
            } else {
                device.setLast_state(true);
            }
            device.setBrightness(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_BRIGHTNESS)));
            device.setColor(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_COLOR)));
            device.setSharedUsers(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_SHARED_USERS)));
            device.setWifi_name(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_SSID)));
       //     device.setShared_with(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_SHARED_NAMES)));
            device.setWifi_password(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Device.COLUMN_NAME_WIFI_PWD)));


            devices.add(device);
            System.out.println("current element " );
            cursor.moveToNext();
        }



        return devices;
    }

}
