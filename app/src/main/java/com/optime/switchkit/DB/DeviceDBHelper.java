package com.optime.switchkit.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rmadan on 7/5/2015.
 */
public class DeviceDBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ChatListReader.db";


    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_DEVICE_TABLE =
            "CREATE TABLE " + FeedReaderContract.Device.TABLE_NAME + " (" +
                    FeedReaderContract.Device.COLUMN_NAME_DID + TEXT_TYPE + " PRIMARY KEY " + COMMA_SEP +//" INTEGER PRIMARY KEY AUTOINCREMENT," +
                    FeedReaderContract.Device.COLUMN_NAME_SHARED_USERS + INTEGER_TYPE + COMMA_SEP +
                    FeedReaderContract.Device.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    FeedReaderContract.Device.COLUMN_NAME_COLOR + TEXT_TYPE + COMMA_SEP +
                    FeedReaderContract.Device.COLUMN_NAME_SHARED_NAMES + TEXT_TYPE + COMMA_SEP +
                    FeedReaderContract.Device.COLUMN_NAME_WIFI_PWD + TEXT_TYPE + COMMA_SEP +
                    FeedReaderContract.Device.COLUMN_NAME_SSID + TEXT_TYPE + COMMA_SEP +
                    FeedReaderContract.Device.COLUMN_NAME_STATE + INTEGER_TYPE + COMMA_SEP +
                    FeedReaderContract.Device.COLUMN_NAME_BRIGHTNESS + INTEGER_TYPE +
                    " )";

    private static final String SQL_DELETE_DEVICE_TABLE =
            "DROP TABLE IF EXISTS " + FeedReaderContract.Device.TABLE_NAME;

    public DeviceDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        System.out.println(SQL_CREATE_DEVICE_TABLE);
        db.execSQL(SQL_CREATE_DEVICE_TABLE);


    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_DEVICE_TABLE);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}

