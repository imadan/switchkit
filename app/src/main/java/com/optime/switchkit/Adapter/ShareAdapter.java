package com.optime.switchkit.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.optime.switchkit.DB.DeviceDBHelper;
import com.optime.switchkit.DB.DeviceDBUtility;
import com.optime.switchkit.Model.Device;
import com.optime.switchkit.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by rmadan on 7/19/2015.
 */
public class ShareAdapter extends BaseAdapter implements View.OnClickListener {

    /*********** Declare Used Variables *********/
    private Context context;
    private ArrayList<Device> devices;
    ArrayList<Boolean> isSelected;
    Device device;

    DeviceDBHelper deviceDBHelper;
    DeviceDBUtility deviceDBUtility;

    public Resources res;
    //ListModel tempValues=null;
    int i=0;
    int listType = 1; //1- earned, 2-redeemed

    /*************  CustomAdapter Constructor *****************/
    public ShareAdapter(Context context, ArrayList<Device> devices) {

        /********** Take passed values **********/
        this.context = context;
        this.devices = devices;
        deviceDBUtility = new DeviceDBUtility();
        deviceDBHelper = new DeviceDBHelper(context);

    }

    public void SetLists(ArrayList<Device> devices)
    {
        this.devices = devices;

    }
    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(devices.size()<=0)
            return 1;
        return devices.size();
    }

    public Object getItem(int position) {
        return position;
    }


    void UpdateDeviceState(boolean state, String did)
    {
        deviceDBUtility.UpdateDeviceState(deviceDBHelper, did, state);
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView name;
        public TextView members;
        public ImageView bulbBrightness;
        public CheckBox state;
        public RelativeLayout bulbLayout;


    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        final ViewHolder holder;

        if(convertView==null){

            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.share_element, null);




            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();


            holder.name = (TextView)vi.findViewById(R.id.device_name);
            holder.state = ( CheckBox)vi.findViewById(R.id.device_state);


            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(devices.size()<=0)
        {
            //	holder.name.setText("No Data");

        }
        else
        {
            device = devices.get(position);
            holder.name.setText(device.getDevice_name());
            holder.state.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (devices.get(position).isLast_state()) {
                        holder.state.setChecked(false);
                        devices.get(position).setLast_state(false);
                    } else {
                        holder.state.setChecked(true);
                        devices.get(position).setLast_state(true);
                    }

                }
            });
            //      vi.setOnClickListener(new OnItemClickListener( position ));
        }
        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener {
        private int mPosition;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {
/*
            Intent chatActivity = new Intent(context, ListLinksActivity.class);
            chatActivity.putExtra("lid", devices.get(mPosition).getLid());
            chatActivity.putExtra("s_code", devices.get(mPosition).getSettings_code());
            chatActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(chatActivity);
  */          //arg0.getContext().startActivity(chatActivity);
            //CustomListViewAndroidExample sct = (CustomListViewAndroidExample)activity;

            /****  Call  onItemClick Method inside CustomListViewAndroidExample Class ( See Below )****/

            //sct.onItemClick(mPosition);
        }
    }


    class SaveFile extends AsyncTask<String, String, String> {

        String responseString;
        String data = null;
        String urlLink, localPath;
        int type;

        SaveFile(String url, String localPath, int type)
        {
            this.urlLink = url;
            this.localPath = localPath;
            this.type = type;
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();


            try {
                URL url = new URL(urlLink);
                //Log.i("FILE_NAME", "File name is " + fileName);
                Log.i("FILE_URLLINK", "File URL is " + url);
                URLConnection connection = url.openConnection();
                connection.connect();
                // this will be useful so that you can show a typical 0-100% progress bar
                int fileLength = connection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(localPath);

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;

                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("ERROR ON DOWNLOADING FILES", "ERROR IS" +e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //Do anything with response..
            //data = "{\"response\":" + data + "}";

        }
    }

}
