package com.optime.switchkit.Activity;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.Message;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;

import java.net.URISyntaxException;

/**
 * Created by rmadan on 7/2/2015.
 */
public class MQTTActivity {
    MQTT mqtt = new MQTT();
    BlockingConnection connection;
    public void SetHost() {

        try {
            mqtt.setHost("mqtt://test.mosquitto.org");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void Subscribe()
    {
        connection = mqtt.blockingConnection();
        try {
            connection.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Topic[] topics = {new Topic("foo", QoS.AT_LEAST_ONCE)};
        try {
            byte[] qoses = connection.subscribe(topics);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Publish()
    {
        try {
            connection.publish("foo", "Hello".getBytes(), QoS.AT_LEAST_ONCE, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Recieve()
    {
        Message message = null;
        try {
            message = connection.receive();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(message.getTopic());
        byte[] payload = message.getPayload();
// process the message then:
        message.ack();
    }
}
