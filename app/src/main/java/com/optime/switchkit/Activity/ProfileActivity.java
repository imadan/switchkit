package com.optime.switchkit.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.optime.switchkit.R;
import com.optime.switchkit.Utility.CommonConstants;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by rmadan on 7/7/2015.
 */
public class ProfileActivity extends Activity {

    EditText fNameView, lNameView, phoneView;
    TextView passView, emailView;
    Button submitButton;

    String fName, lName, email, phone, oldPass, pass, cpass;
    String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_screen);

        InitializeViews();
        InitializeListeners();
        GetValuesFromPreferences();
        PopulateViews();
    }

    void InitializeViews()
    {
        fNameView = (EditText)findViewById(R.id.first_name);
        lNameView = (EditText)findViewById(R.id.last_name);
        emailView = (TextView)findViewById(R.id.email);
        phoneView = (EditText)findViewById(R.id.mobile);
        passView = (TextView)findViewById(R.id.pass);


        submitButton = (Button)findViewById(R.id.submit);
    }

    void InitializeListeners()
    {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fName = fNameView.getText().toString();
                lName = lNameView.getText().toString();
                phone = phoneView.getText().toString();
                email = emailView.getText().toString();
                pass = passView.getText().toString();

                provider = "SWITCH";
                new UpdateProfile().execute();

            }
        });

        passView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowForgotPwdPopUp();

            }
        });
    }


    public void ShowForgotPwdPopUp()
    {
        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfileActivity.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Change Password");

        // Setting Dialog Message
        //alertDialog.setMessage("");
        final EditText oldPassword = new EditText(ProfileActivity.this);
        final EditText newPassword = new EditText(ProfileActivity.this);
        final EditText confirmPassword = new EditText(ProfileActivity.this);
        final LinearLayout dialogLayout = new LinearLayout(ProfileActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);


        dialogLayout.setLayoutParams(lp);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        oldPassword.setHint("Old password");
        newPassword.setHint("New password");
        confirmPassword.setHint("Confirm password");
        dialogLayout.addView(oldPassword);
        dialogLayout.addView(newPassword);
        dialogLayout.addView(confirmPassword);
        alertDialog.setView(dialogLayout);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        if (!confirmPassword.getText().toString().equals(newPassword.getText().toString())) {
                            Toast.makeText(ProfileActivity.this, "Password and confirm password do not match", Toast.LENGTH_SHORT).show();
                        } else {
                            oldPass = oldPassword.getText().toString();
                            cpass = confirmPassword.getText().toString();
                        }
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
    }


    void GetValuesFromPreferences()
    {
        SharedPreferences userSettings;
        userSettings = ProfileActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        email = userSettings.getString(CommonConstants.USER_EMAIL, "");
        lName = userSettings.getString(CommonConstants.USER_LNAME, "");
        fName = userSettings.getString(CommonConstants.USER_FNAME, "");
        phone = userSettings.getString(CommonConstants.USER_MOBILE, "");

    }

    void PopulateViews()
    {
        emailView.setText(email);
        lNameView.setText(lName);
        fNameView.setText(fName);
        phoneView.setText(phone);
        passView.setText("23432");


    }


    class UpdateProfile extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";
        int statusCode = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();



            try{
                post = new HttpPost(CommonConstants.SERVER_URL + "/v1/user/update");
                json.put("firstname", fName);
                json.put("email", email);
                json.put("lastname", lName);
                json.put("mobile", phone);
                if (cpass != null && !cpass.equals("")) {
                    json.put("old_password", oldPass);
                    json.put("new_password", cpass);
                }
                //json.put("password", pass);


                System.out.println("body " + json.toString());
                ByteArrayEntity se = new ByteArrayEntity(json.toString().getBytes("UTF8"));
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);
				/*Checking response */
				/*if(response!=null){
	                InputStream in = response.getEntity().getContent(); //Get the data in the entity
				 */
                String responseString;
                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(response.getEntity());
                Log.i("tag", responseString);


                System.out.println("sign up string " + responseString);

                data = responseString;
                System.out.println("returned " + data);

                return data;




            }
            catch(Exception e){
                e.printStackTrace();
                // createDialog("Error", "Cannot Estabilish Connection");
            }


            return null;
        }

        protected void onPostExecute(String result) {


            if (statusCode == 200) {
                //Toast.makeText(context, url, Toast.LENGTH_SHORT).show();
                if (data != null) {
                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(CommonConstants.USER_EMAIL, email);
                    editor.putString(CommonConstants.USER_FNAME, fName);
                    editor.putString(CommonConstants.USER_LNAME, lName);
                    editor.putString(CommonConstants.USER_MOBILE, phone);

                    editor.commit();
                   ProfileActivity.this.finish();
                } else {

                }

            }
        }

    }

}
