package com.optime.switchkit.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.Gson;
import com.optime.switchkit.Model.LoginResponse;
import com.optime.switchkit.Model.User;
import com.optime.switchkit.R;
import com.optime.switchkit.Utility.CommonConstants;
import com.optime.switchkit.Utility.Utility;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 * Created by rmadan on 6/26/2015.
 */
public class LoginActivity extends FragmentActivity implements
        View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    MQTTActivity mqttActivity;

    CallbackManager callbackManager;

    private LoginButton loginButton;
    int login_type;

    String device_id;
    String device_token;
    int showPass = 0;


    /* Client used to interact with Google APIs. */
    static GoogleApiClient mGoogleApiClient;
    private SignInButton btnSignIn;
    public Button mockGoogleButton, mockFBButton;

    private static final int RC_SIGN_IN = 0;
    String googleId;
    private boolean mSignInClicked;

    private ConnectionResult mConnectionResult;

    SharedPreferences userSettings;
    private static SharedPreferences mSharedPreferences;
    /* A flag indicating that a PendingIntent is in progress and prevents
     * us from starting further intents.
     */
    private boolean mIntentInProgress;

    EditText usernameEditText, phoneEditText;
    Button signIn, signUp;
    TextView forgotPwd;
    CheckBox showPassword;


    String email, pwd, name, fbId, access_token, image, fname, lname, provider, provider_id, mobile;
    ProgressDialog progress;
    ViewPager viewPager;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.new_login_screen);

 /*       mqttActivity = new MQTTActivity();
        mqttActivity.SetHost();
        mqttActivity.Subscribe();
        new Recieve().execute();
        System.out.println("sending now");
        mqttActivity.Publish();
        System.out.println("sent");
*/

        context = LoginActivity.this;
        progress = new ProgressDialog(this);


        InitializeViews();
        InitializeListners();
        GetValuesFromPreferences();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends"));
        callbackManager = CallbackManager.Factory.create();

        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                object = response.getJSONObject();
                                Log.v("LoginActivity", response.toString());
                                try {
                                    fbId = object.getString("id");
                                    provider_id = fbId;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fname = object.getString("first_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    email = object.getString("email");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                try {
                                    lname = object.getString("last_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    name = object.getString("name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    image = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                login_type = 1;
                                //  Intent intent = new Intent(context, CreateUniqueName.class);
                                //  startActivityForResult(intent, 150);
                                provider = "FACEBOOK";
                                new LoginFB().execute();

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday, picture.width(9000), first_name, last_name");
                request.setParameters(parameters);
                request.executeAsync();
                System.out.println(loginResult);
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        /*
        fbLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Session currentSession = Session.getActiveSession();
                if (currentSession == null || currentSession.getState().isClosed()) {
                    Session session = new Session.Builder(context).build();
                    Session.setActiveSession(session);
                    currentSession = session;
                }

                if (currentSession.isOpened()) {
                    Session.openActiveSession(context, true, new Session.StatusCallback() {

                        @Override
                        public void call(final Session session, SessionState state,
                                         Exception exception) {

                            if (session.isOpened()) {

                                Request.executeMeRequestAsync(session,
                                        new Request.GraphUserCallback() {

                                            @Override
                                            public void onCompleted(GraphUser user,
                                                                    Response response) {
                                                if (user != null) {

                                                    email = user.getProperty("email").toString();
                                                    name = user.getName();
                                                    fname = user.getFirstName();
                                                    lname = user.getLastName();
                                                    //String lastName = user.getLastName();
                                                    fbId = user.getId();
                                                    // System.out.println("User is " + email);
                                                    progress.setMessage("Logging in :) ");
                                                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                                    progress.setIndeterminate(true);
                                                    progress.show();


                                                    new Request(
                                                            session,
                                                            "/" + fbId + "/picture",
                                                            null,
                                                            HttpMethod.GET,
                                                            new Request.Callback() {
                                                                public void onCompleted(Response response) {
                                                                    System.out.println("image link is " + response.getConnection().getURL());
                                                                    image = response.getConnection().getURL().toString();
                                                                    new LoginFB().execute();

                                                                }
                                                            }
                                                    ).executeAsync();

                                                }
                                            }
                                        });
                            }
                        }
                    });
                    // Do whatever u want. User has logged in

                } else if (!currentSession.isOpened()) {
                    // Ask for username and password
                    Session.OpenRequest op = new Session.OpenRequest((Activity) context);

                    op.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
                    op.setCallback(null);

                    List<String> permissions = new ArrayList<String>();
                    permissions.add("publish_stream");
                    permissions.add("publish_actions");
                    permissions.add("user_likes");
                    permissions.add("email");
                    permissions.add("user_birthday");
                    op.setPermissions(permissions);

                    Session session = new Session.Builder(context).build();
                    Session.setActiveSession(session);
                    session.openForPublish(op);
                }

            }
        });
*/    }

    void GetDeviceParameters()
    {

        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        device_id = telephonyManager.getDeviceId();
    }

    void SetValues()
    {

    }

    void InitializeViews()
    {
        usernameEditText = (EditText)findViewById(R.id.username_edit_text);
        phoneEditText = (EditText)findViewById(R.id.phone_edit_text);
        signIn = (Button)findViewById(R.id.signin_button);
        signUp = (Button)findViewById(R.id.sign_up);
        forgotPwd = (TextView)findViewById(R.id.forgot_pwd_text);
        showPassword = (CheckBox)findViewById(R.id.show_password);

        userSettings = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        ;
    }

    void InitializeListners()
    {
        signIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                email = usernameEditText.getText().toString();
                pwd = phoneEditText.getText().toString();
                Intent intent = new Intent(LoginActivity.this, DeviceLandingActivity.class);
                //startActivity(intent);

                if (email.equals("") || pwd.equals("")) {
                    Toast.makeText(context, "Please enter values in the mandatory fields", Toast.LENGTH_LONG).show();
                    return;
                }

                String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
                if (!email.matches(EMAIL_REGEX)) {
                    Toast.makeText(LoginActivity.this, "Invalid email", Toast.LENGTH_SHORT).show();
                    return;
                }

                login_type = 3;

                new Login().execute();


            }
        });


        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShowForgotPwdPopUp();
            }
        });

        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showPass == 0) {
                    showPass = 1;
                    showPassword.setChecked(true);
                    phoneEditText.setInputType(InputType.TYPE_CLASS_TEXT );
                    phoneEditText.setSelection(phoneEditText.getText().length());
                } else {
                    showPass = 0;
                    showPassword.setChecked(false);
                    phoneEditText.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    phoneEditText.setSelection(phoneEditText.getText().length());
                }
            }
        });
    }


    public void ShowForgotPwdPopUp()
    {
        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Reset Password");

        // Setting Dialog Message
        //alertDialog.setMessage("");
        final EditText dialogEmail = new EditText(LoginActivity.this);
        final EditText dialogMobile = new EditText(LoginActivity.this);
        final LinearLayout dialogLayout = new LinearLayout(LoginActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);


        dialogLayout.setLayoutParams(lp);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        dialogEmail.setHint("Enter email");
        dialogMobile.setHint("Enter mobile number");
        dialogLayout.addView(dialogEmail);
        dialogLayout.addView(dialogMobile);
        alertDialog.setView(dialogLayout);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        new ForgotPwd(dialogEmail.getText().toString(), dialogMobile.getText().toString()).execute();

                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public void onConnected(Bundle bundle) {
        mSignInClicked = false;
        //  Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();

        // Get user's information
        getProfileInformation();

        // Update the UI after signin
        // updateUI(true);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    /**
     * Sign-in into google
     * */
    private void signInWithGplus() {

        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_sign_in:
                // Signin button clicked



                if (Utility.ShowNetworkDialog(context)) {
                    signInWithGplus();
                }
                break;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }

    }

    private void resolveSignInError() {
        if (mConnectionResult != null) {
            if (mConnectionResult.hasResolution()) {
                try {
                    mIntentInProgress = true;
                    mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
                } catch (IntentSender.SendIntentException e) {
                    mIntentInProgress = false;
                    mGoogleApiClient.connect();
                }
            }
        }
        else {
            if (mSignInClicked == true) {
                mGoogleApiClient.connect();

            }
        }
    }


    void GetValuesFromPreferences()
    {

        access_token = userSettings.getString(CommonConstants.USER_ACCESS_TOKEN, null);

    }


    protected void onStart() {
        super.onStart();
        //mGoogleApiClient.connect();
    }


    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {



        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }

        } else {
            callbackManager.onActivityResult(requestCode, responseCode, intent);
        }

    }



    /**
     * Fetching user's information name, email, profile pic
     * */
    private void getProfileInformation() {
        try {

            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                String plusemail = Plus.AccountApi.getAccountName(mGoogleApiClient);

                Log.e("details", "Name: " + personName + ", plusProfile: "
                        + personGooglePlusProfile + ", email: " + plusemail
                        + ", Image: " + personPhotoUrl);

                name = personName;
                email = plusemail;
                googleId = (personGooglePlusProfile.split("/"))[(personGooglePlusProfile.split("/")).length - 1];
                provider_id = googleId;
                provider = "GOOGLE";

                login_type = 2;
                if (Utility.ShowNetworkDialog(context)) {
                    progress.setMessage("Logging in :) ");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.show();
                    // Intent intent = new Intent(context, CreateUniqueName.class);
                    // startActivityForResult(intent, 150);
                    //      new LoginGoogle().execute();
                        new LoginFB().execute();
                }



            } else {
                Toast.makeText(getApplicationContext(),
                        "Person information is null", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
        public void call(Session session, SessionState state, Exception exception) {
        }
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class Recieve extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setMessage("Logging in :) ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            System.out.println("ready to recieve");
            mqttActivity.Recieve();
            System.out.println("recieved");
            return null;
        }
    }

    class Login extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";
        int statusCode = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setMessage("Logging in :) ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();


            try {
                post = new HttpPost(CommonConstants.SERVER_URL + "/v1/login/validateuser");
                json.put("email", email);
                json.put("password", pwd);


                ByteArrayEntity se = new ByteArrayEntity(json.toString().getBytes("UTF8"));
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);
				/*Checking response */
				/*if(response!=null){
	                InputStream in = response.getEntity().getContent(); //Get the data in the entity
				 */
                String responseString;
                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(response.getEntity());
                Log.i("tag", responseString);


                System.out.println("sign up string " + responseString);

                data = responseString;
                System.out.println("returned " + data);

                return data;


            } catch (Exception e) {
                e.printStackTrace();
                // createDialog("Error", "Cannot Estabilish Connection");
            }


            return null;
        }

        protected void onPostExecute(String result) {


            if (progress != null) {
                progress.dismiss();
            }
            if (statusCode == 200) {

                System.out.println("sign up string " + data);
                Gson gson = new Gson();
                LoginResponse loginResponse = gson.fromJson(data, LoginResponse.class);
                if (loginResponse.getResult().equals("UserValidationSuccessFul")) {

                    if (loginResponse.getUser_id() != null) {
                        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(CommonConstants.USER_ID, loginResponse.getUser_id());
                        editor.putString(CommonConstants.USER_EMAIL, loginResponse.getEmail());
                        editor.putString(CommonConstants.USER_FNAME, loginResponse.getFirstname());
                        editor.putString(CommonConstants.USER_LNAME, loginResponse.getLastname());
                        editor.putString(CommonConstants.USER_MOBILE, loginResponse.getMobile());
                        editor.putString(CommonConstants.LOGIN_PROVIDER, loginResponse.getProvider());

                        editor.commit();

                        Intent intent = new Intent(LoginActivity.this, DeviceLandingActivity.class);

                        intent.putExtra("just_logged", 1);
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }
                }

                //Toast.makeText(context, url, Toast.LENGTH_SHORT).show();UserValidationSuccessFull
                //   if (data != null) {

                //data = "{\"response\":" + data + "}";

                //    }

                //    else {

                //    }

            }

        }

    }

    class LoginFB extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";
        int statusCode = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setMessage("Logging in :) ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();



            try{
                post = new HttpPost(CommonConstants.SERVER_URL + "/v1/login/createuser");
                json.put("email", email);
                if (fname == null) {
                    fname = "";
                }
                if (lname == null) {
                    lname = "";
                }
                if (provider == null) {
                    provider = "";
                }
                if (provider_id == null) {
                    provider_id = "";
                }


                json.put("firstname",fname );
                json.put("lastname", lname);
                json.put("mobile", "");
                json.put("provider", provider);
                json.put("provider_id", provider_id);
                json.put("access_token", "");
                json.put("password", "");


                ByteArrayEntity se = new ByteArrayEntity(json.toString().getBytes("UTF8"));
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);
				/*Checking response */
				/*if(response!=null){
	                InputStream in = response.getEntity().getContent(); //Get the data in the entity
				 */
                String responseString;
                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(response.getEntity());
                Log.i("tag", responseString);


                System.out.println("sign up string " + responseString);

                data = responseString;
                System.out.println("returned " + data);

                return data;




            }
            catch(Exception e){
                e.printStackTrace();
                // createDialog("Error", "Cannot Estabilish Connection");
            }


            return null;
        }

        protected void onPostExecute(String result) {


            if (progress != null) {
                progress.dismiss();
            }
            if (statusCode == 201 || statusCode == 200) {

                Gson gson = new Gson();
                User user = gson.fromJson(data, User.class);
                if (user.getUser_id() != null) {
                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(CommonConstants.USER_ID, user.getUser_id());
                    editor.putString(CommonConstants.USER_EMAIL, email);
                    editor.putString(CommonConstants.USER_FNAME, fname);
                    editor.putString(CommonConstants.USER_LNAME, lname);
                    editor.putString(CommonConstants.USER_MOBILE, mobile);
                    editor.putString(CommonConstants.LOGIN_PROVIDER, provider);

                    editor.commit();

                    Intent intent = new Intent(LoginActivity.this, DeviceLandingActivity.class);
                    intent.putExtra("just_logged", 1);
                    startActivity(intent);
                    LoginActivity.this.finish();
                }

            }

            System.out.println("sign up string " + data);
            Gson gson = new Gson();

            //    }

            //    else {

            //    }

        }

    }


    class ForgotPwd extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";
        int statusCode = 0;
        String email, mobile;

        ForgotPwd(String email, String mobile)
        {
            this.email = email;
            this.mobile = mobile;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setMessage("Logging in :) ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();



            try{
                post = new HttpPost(CommonConstants.SERVER_URL + "/v1/login/forgot");
                json.put("email", email);
                json.put("mobile", "");

                ByteArrayEntity se = new ByteArrayEntity(json.toString().getBytes("UTF8"));
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);
				/*Checking response */
				/*if(response!=null){
	                InputStream in = response.getEntity().getContent(); //Get the data in the entity
				 */
                String responseString;
                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(response.getEntity());
                Log.i("tag", responseString);


                System.out.println("sign up string " + responseString);

                data = responseString;
                System.out.println("returned " + data);

                return data;




            }
            catch(Exception e){
                e.printStackTrace();
                // createDialog("Error", "Cannot Estabilish Connection");
            }


            return null;
        }

        protected void onPostExecute(String result) {


            if (progress != null) {
                progress.dismiss();
            }
            if (statusCode == 201 || statusCode == 200) {

                Gson gson = new Gson();

            }

            //    }

            //    else {

            //    }

        }

    }



}