package com.optime.switchkit.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.optime.switchkit.Adapter.ShareAdapter;
import com.optime.switchkit.DB.DeviceDBHelper;
import com.optime.switchkit.DB.DeviceDBUtility;
import com.optime.switchkit.Model.Device;
import com.optime.switchkit.Model.GenerateOTPResponse;
import com.optime.switchkit.R;
import com.optime.switchkit.Utility.CommonConstants;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by rmadan on 7/19/2015.
 */
public class ShareActivity extends Activity

{
    ListView deviceList;
    Button addButton;
    ArrayList<Device> devices;

    ShareAdapter deviceListAdapter;
    DeviceDBHelper deviceDBHelper;
    DeviceDBUtility deviceDBUtility;

    int addState = 0;
    String recievedMac = "";
    String uid;
    int otp;


    Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_screen);

        InitializeViews();
        InitializeListeners();
        InitializeDB();
        GetValuesFromDB();
        GetValuesFromPreferences();
        PopulateViews();

    }

    public void ShowOTPPopup()
    {
        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ShareActivity.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("OTP");

        // Setting Dialog Message
        //alertDialog.setMessage("");
        final TextView input = new TextView(ShareActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setText(otp + "");
        alertDialog.setView(input);

        // Setting Positive "Yes" Button
        alertDialog.setNegativeButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
    }

    void GetValuesFromPreferences()
    {
        SharedPreferences userSettings;
        userSettings = ShareActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        uid = userSettings.getString(CommonConstants.USER_ID, "");

    }
    void InitializeViews()
    {
        deviceList = (ListView)findViewById(R.id.device_list);
        addButton = (Button)findViewById(R.id.share_view);
    }

    void InitializeListeners()
    {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String d = "";
                for (int i=0; i<devices.size(); i++) {
                    if (devices.get(i).isLast_state()) {
                        d = d + devices.get(i).getDevice_id();
                        if (i != devices.size() - 1) {
                            d = d + ",";
                        }
                    }
                }
                new ShareDevices(d.split(",")).execute();
            }
        });
    }
    void PopulateViews()
    {
        if (devices != null && devices.size()>0) {
            deviceListAdapter = new ShareAdapter(ShareActivity.this, devices);
            deviceList.setAdapter(deviceListAdapter);

        }

    }

    void GetValuesFromDB()
    {
        devices = deviceDBUtility.GetDevice(deviceDBHelper, "");
        for (int i=0; i<devices.size(); i++) {
            devices.get(i).setLast_state(false);
        }
    }

    void InitializeDB()
    {
        deviceDBUtility = new DeviceDBUtility();
        deviceDBHelper = new DeviceDBHelper(ShareActivity.this);



    }
    public class JsonStruct {
        public String[] device_id;
    }


    class ShareDevices extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";
        int statusCode = 0;
        ProgressDialog progress;
        String shared[];

        ShareDevices(String []shared)
        {
            this.shared = shared;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(ShareActivity.this);
            progress.setMessage("Fetching OTP ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            try{
                post = new HttpPost(CommonConstants.SERVER_URL + "/v1/user/" + uid + "/otp");
                JsonStruct j = new JsonStruct();
                j.device_id = shared;
                Gson g = new Gson();
                ByteArrayEntity se = new ByteArrayEntity(g.toJson(j).getBytes("UTF8"));
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);
				/*Checking response */
				/*if(response!=null){
	                InputStream in = response.getEntity().getContent(); //Get the data in the entity
				 */
                String responseString;
                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(response.getEntity());
                Log.i("tag", responseString);


                System.out.println("sign up string " + responseString);

                data = responseString;
                System.out.println("returned " + data);

                return data;
            }
            catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String result) {


            if (progress != null) {
                progress.dismiss();
            }
            if (statusCode == 201 || statusCode == 200) {

                System.out.println("sign up string " + data);
                Gson gson = new Gson();
                GenerateOTPResponse generateOTPResponse = gson.fromJson(data, GenerateOTPResponse.class);
                if (generateOTPResponse != null) {
                    otp = generateOTPResponse.getOtp();
                    ShowOTPPopup();
                }

            }


            //    }

            //    else {

            //    }

        }

    }

}
