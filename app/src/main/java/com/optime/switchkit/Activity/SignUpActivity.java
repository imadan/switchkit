package com.optime.switchkit.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.optime.switchkit.Model.User;
import com.optime.switchkit.R;
import com.optime.switchkit.Utility.CommonConstants;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by rmadan on 6/27/2015.
 */
public class SignUpActivity extends Activity {

    EditText fNameView, lNameView, emailView, phoneView, passView, cPassView;
    Button submitButton;

    String fName, lName, email, phone, pass, cpass;
    String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_screen);

        InitializeViews();
        InitializeListeners();
    }

    void InitializeViews()
    {
        fNameView = (EditText)findViewById(R.id.first_name);
        lNameView = (EditText)findViewById(R.id.last_name);
        emailView = (EditText)findViewById(R.id.email);
        phoneView = (EditText)findViewById(R.id.mobile);
        passView = (EditText)findViewById(R.id.pass);
        cPassView = (EditText)findViewById(R.id.cpass);

        submitButton = (Button)findViewById(R.id.submit);
    }

    void InitializeListeners()
    {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fName = fNameView.getText().toString();
                lName = lNameView.getText().toString();
                phone = phoneView.getText().toString();
                email = emailView.getText().toString();
                pass = passView.getText().toString();
                cpass = cPassView.getText().toString();

                if (fName.isEmpty() || phone.isEmpty() || email.isEmpty() || pass.isEmpty()) {
                    Toast.makeText(SignUpActivity.this, "Please enter all the mandatory fields", Toast.LENGTH_SHORT).show();
                    return;
                } else if (!pass.equals(cpass)) {
                    Toast.makeText(SignUpActivity.this, "Password and confirm password do not match", Toast.LENGTH_SHORT).show();
                    return;
                }


                provider = "SWITCH";
                new SignUp().execute();

            }
        });
    }


    class SignUp extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";
        int statusCode = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();



            try{
                post = new HttpPost(CommonConstants.SERVER_URL + "/v1/login/createuser");
                json.put("firstname", fName);
                json.put("email", email);
                json.put("lastname", lName);
                json.put("mobile", phone);
                json.put("password", pass);
                json.put("provider", provider);
                json.put("provider_id", "");
                json.put("access_token", "");


                System.out.println("body " + json.toString());
                ByteArrayEntity se = new ByteArrayEntity(json.toString().getBytes("UTF8"));
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);
				/*Checking response */
				/*if(response!=null){
	                InputStream in = response.getEntity().getContent(); //Get the data in the entity
				 */
                String responseString;
                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(response.getEntity());
                Log.i("tag", responseString);


                System.out.println("sign up string " + responseString);

                data = responseString;
                System.out.println("returned " + data);

                return data;




            }
            catch(Exception e){
                e.printStackTrace();
                // createDialog("Error", "Cannot Estabilish Connection");
            }


            return null;
        }

        protected void onPostExecute(String result) {


            if (statusCode == 201) {
                //Toast.makeText(context, url, Toast.LENGTH_SHORT).show();
                if (data != null) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(data, User.class);

                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(CommonConstants.USER_ID, user.getUser_id());
                    editor.putString(CommonConstants.USER_EMAIL, email);
                    editor.putString(CommonConstants.USER_FNAME, fName);
                    editor.putString(CommonConstants.USER_LNAME, lName);
                    editor.putString(CommonConstants.USER_MOBILE, phone);
                    editor.putString(CommonConstants.LOGIN_PROVIDER, "SWITCH");

                    editor.commit();

                    Intent intent = new Intent(SignUpActivity.this, DeviceLandingActivity.class);
                    intent.putExtra("just_logged", 1);
                    startActivity(intent);

                } else {

                }

            }
        }

    }

}
