package com.optime.switchkit.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.optime.switchkit.R;
import com.optime.switchkit.Utility.CommonConstants;

/**
 * Created by rmadan on 6/14/2015.
 */
public class SettingsActivity extends Activity{

    RelativeLayout profileLayout, aboutusLayout, faqLayout;
    TextView emailView, changePwdView;

    ProgressDialog progress;

    String email, user_token;
    int login_type = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_screen);

        InitializeViews();
        InitializeListeners();
        GetValuesFromPreferences();
        PopulateViews();
    }

    void InitializeViews()
    {
        profileLayout = (RelativeLayout)findViewById(R.id.profile_layout);
        aboutusLayout = (RelativeLayout)findViewById(R.id.about_us_layout);
        faqLayout = (RelativeLayout)findViewById(R.id.faq_layout);
    }

    void InitializeListeners()
    {

        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SettingsActivity.this, ProfileActivity.class);
                startActivity(intent);



            }
        });

        aboutusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Intent intent = new Intent(SettingsActivity.this, TermsActivity.class);
                //   startActivity(intent);
            }
        });


        faqLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Intent intent = new Intent(SettingsActivity.this, PolicyActivity.class);
                //   startActivity(intent);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            GetValuesFromPreferences();
            PopulateViews();
        }
    }

    void PopulateViews()
    {

    }

    void GetValuesFromPreferences()
    {
        SharedPreferences userSettings;
        userSettings = SettingsActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        email = userSettings.getString(CommonConstants.USER_EMAIL, "");


    }



}
