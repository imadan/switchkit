package com.optime.switchkit.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.optime.switchkit.DB.DeviceDBHelper;
import com.optime.switchkit.DB.DeviceDBUtility;
import com.optime.switchkit.Model.Device;
import com.optime.switchkit.R;

import java.util.ArrayList;

/**
 * Created by rmadan on 7/5/2015.
 */
public class DeviceSettingActivity extends Activity {
    SeekBar brightnessControl;
    DeviceDBHelper deviceDBHelper;
    DeviceDBUtility deviceDBUtility;
    Device device;

    String did;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_setting_screen);


        InitializeViews();
        InitializeListners();
        GetValuesFromIntent();
        InitializeDB();
        GetValuesFromDB();
        getActionBar().setTitle(device.getDevice_name());

    }

    void InitializeViews()
    {
        brightnessControl = (SeekBar)findViewById(R.id.brightness_bar);
    }

    void InitializeListners()
    {

        brightnessControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                progressChanged = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(DeviceSettingActivity.this, "seek bar progress:" + progressChanged,
                        Toast.LENGTH_SHORT).show();
                UpdateDeviceBrightness(progressChanged);
            }
        });
    }

    void GetValuesFromIntent()
    {
        did = getIntent().getStringExtra("did");
    }

    void GetValuesFromDB()
    {
        ArrayList<Device> devices = deviceDBUtility.GetDevice(deviceDBHelper, did);
        device = devices.get(0);

        brightnessControl.setProgress(device.getBrightness());
      //  getActionBar().setTitle(device.getName());
    }

    void InitializeDB()
    {
        deviceDBUtility = new DeviceDBUtility();
        deviceDBHelper = new DeviceDBHelper(DeviceSettingActivity.this);
    }

    void UpdateDeviceBrightness(int brightness)
    {
        deviceDBUtility.UpdateDeviceBrightness(deviceDBHelper, did, brightness);
    }

    void UpdateDeviceName(String name)
    {
        getActionBar().setTitle(name);
        deviceDBUtility.UpdateDeviceName(deviceDBHelper, did, name);
    }

    void ShowSharedNamesDialog()
    {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                DeviceSettingActivity.this);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Shared with");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                DeviceSettingActivity.this,
                android.R.layout.simple_list_item_1);
        String []names = null;// = device.getShared_with().split(",");
        for (int i=0; i<names.length; i++) {
            arrayAdapter.add(names[i]);
        }

        builderSingle.setNegativeButton("OK",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                null);
        builderSingle.show();
    }

    void ShowRenameDialog()
    {
        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DeviceSettingActivity.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Rename");

        // Setting Dialog Message
        //alertDialog.setMessage("");
        final EditText input = new EditText(DeviceSettingActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setText(device.getDevice_name());
        alertDialog.setView(input);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        UpdateDeviceName(input.getText().toString());
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.device_setting_menu, menu);

        // Associate searchable configuration with the SearchView

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        Context context = DeviceSettingActivity.this;
        switch (item.getItemId()) {
            case android.R.id.home:
                DeviceSettingActivity.this.finish();
                return false;
            case R.id.wifi:
                return false;
            case R.id.shared_with:
                ShowSharedNamesDialog();
                return false;
            case R.id.rename:
                ShowRenameDialog();
                return false;
        }
        return false;
    }

}