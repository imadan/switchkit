package com.optime.switchkit.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.optime.switchkit.Adapter.WifiAdapter;
import com.optime.switchkit.DB.DeviceDBHelper;
import com.optime.switchkit.DB.DeviceDBUtility;
import com.optime.switchkit.Model.Device;
import com.optime.switchkit.Model.TCPScoketResponse;
import com.optime.switchkit.Model.User;
import com.optime.switchkit.R;
import com.optime.switchkit.Utility.CommonConstants;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import static android.net.wifi.WifiManager.EXTRA_SUPPLICANT_CONNECTED;

/**
 * Created by rmadan on 7/11/2015.
 */
public class WifiConnectivityActivity extends Activity {
    WifiManager wifi;
    int size = 0;
    int selectedPosition = -1;
    int connectedToDevice = 1;
    String recievedMac = "";
    Socket socket;
    static int oldNetId = -1;
    static int deviceNetId = -1;
    static int routerNetId = -1;
    String ssidForDevice;
    String password;
    String did;
    String name;
    String uid;
    String uname;

    String socketResponse = "";
    static int connectivityState = -1; //0-scanned, 1-connecting to router, 2- connecting to device, 3-connecting to old network
    int alreadyRegistered = 0;
    int success = 0;
    List<ScanResult> results;
    ListView wifiList;
    WifiAdapter wifiAdapter;

    DeviceDBHelper deviceDBHelper;
    DeviceDBUtility deviceDBUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifi_screen);

        recievedMac = getIntent().getStringExtra("mac");
        did = getIntent().getStringExtra("did");
        wifiList = (ListView) findViewById(R.id.wifi_list);

        wifiList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPosition = position;

                ssidForDevice = results.get(position).SSID;
                ShowPasswordPopUp();

            }
        });
        //makeNotification(WifiConnectivityActivity.this);
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled() == false) {
            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
            wifi.setWifiEnabled(true);
        }
        GetValuesFromPreferences();
        connectivityState = 0;
        if (alreadyRegistered == 0) {
            try {
                registerReceiver(scanReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            } catch (Exception e) {

            }
            wifi.startScan();

            final IntentFilter filters = new IntentFilter();
            filters.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            filters.addAction("android.net.wifi.STATE_CHANGE");
            filters.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
            super.registerReceiver(mMessageReceiver, filters);
            getCurrentNetId(WifiConnectivityActivity.this);
            //ConnectToDevice("SWITCH_C8:93:46:41:D6:39_NODE");
        }
        alreadyRegistered = 1;
    }

    void GetValuesFromPreferences()
    {
        SharedPreferences userSettings;
        userSettings = WifiConnectivityActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        uid = userSettings.getString(CommonConstants.USER_ID, "");
        String lName = userSettings.getString(CommonConstants.USER_LNAME, "");
        String fName = userSettings.getString(CommonConstants.USER_FNAME, "");
        uname = fName + lName;


    }

    String GetDeviceSSID()
    {
        String ssid = null;
        int flag = 0;
        for (int i=0; i<results.size(); i++) {
            if (results.get(i).SSID.toLowerCase().contains("switch")) {
                if (ssid == null) {
                    ssid = results.get(i).SSID;
                } else {
                    flag = 1;
                    ssid = ssid + results.get(i).SSID;
                }
            }
        }
        if (flag == 0) {
            return ssid;
        } else {
            return null;
        }
    }

    void ShowPasswordPopUp()
    {

        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(WifiConnectivityActivity.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Enter password");

        // Setting Dialog Message
        //alertDialog.setMessage("");
        final EditText input = new EditText(WifiConnectivityActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        alertDialog.setView(input);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        password = input.getText().toString();

                        ConnectToRouter(results.get(selectedPosition).SSID, password);
                        //new GetdeviceMac(input.getText().toString()).execute();
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
    }

    void ShowDeviceNamePopUp()
    {

        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(WifiConnectivityActivity.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Enter device name");

        // Setting Dialog Message
        //alertDialog.setMessage("");
        final EditText input = new EditText(WifiConnectivityActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        alertDialog.setView(input);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        name = input.getText().toString();
                        new AddDeviceToServer().execute();
                        //new GetdeviceMac(input.getText().toString()).execute();
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
        System.out.println("pop up shown");
    }

    void AddDeviceToDB(Device device)
    {
        deviceDBUtility = new DeviceDBUtility();
        deviceDBHelper = new DeviceDBHelper(WifiConnectivityActivity.this);

        deviceDBUtility.AddStatusToDB(deviceDBHelper, device);
        WifiConnectivityActivity.this.finish();

    }

    void AddDevice()
    {
        Device device = new Device();

        device.setDevice_id(did);

        device.setDevice_name(name);
        device.setLast_state(true);
        device.setBrightness(60);
        device.setColor(0x880000);
       // device.setShared_with("");
        device.setWifi_password(password);
        device.setWifi_name(ssidForDevice);
        AddDeviceToDB(device);

    }


    private void makeNotification(Context context) {
        Intent intent = new Intent(context, WifiConnectivityActivity.class);
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle("Notification Title")
                .setContentText("Sample Notification Content")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                ;
        Notification n;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            n = builder.build();
        } else {
            n = builder.getNotification();
        }

        n.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

        notificationManager.notify(0, n);
    }


    public void ConnectToDevice(String ssid)
    {

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", ssid);
        wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
//remember id
        int netId = wifiManager.addNetwork(wifiConfig);
        deviceNetId = netId;
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        connectivityState = 2;
        wifiManager.reconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void ConnectToRouter(String ssid, String pwd)
    {

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", ssid);
        wifiConfig.preSharedKey = String.format("\"%s\"", pwd);

        WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
//remember id

        int netId = wifiManager.addNetwork(wifiConfig);
        routerNetId = netId;
        //wifiManager.disableNetwork(wifiManager.getConnectionInfo().getNetworkId());
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        connectivityState = 1;
        wifiManager.reconnect();
        // new Thread(new ValidateWifi()).start();

    }

    void ConnectBack()
    {
        WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
        connectivityState = 3;
        wifiManager.disconnect();
        wifiManager.enableNetwork(oldNetId, true);
        wifiManager.reconnect();
        System.out.println("recieved reconnected");
        WifiConnectivityActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (success == 1) {
                    WifiConnectivityActivity.this.ShowDeviceNamePopUp();
                }
            }//public void run() {
        });


    }

    public BroadcastReceiver scanReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {

            //  unregisterReceiver(scanReciever);
            results = wifi.getScanResults();
            size = results.size();

            WifiAdapter wifiAdapter = new WifiAdapter(WifiConnectivityActivity.this, results);
            wifiList.setAdapter(wifiAdapter);

        }
    };

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();

            System.out.println("action recieved is " + action );
            SupplicantState supl_state=((SupplicantState)intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE));
            if (supl_state != null && supl_state.equals(SupplicantState.COMPLETED)) {
                WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
                if (connectivityState == 1) {

//remember id
                    if (routerNetId == wifiManager.getConnectionInfo().getNetworkId()) {
                        new Thread(new ValidateWifi()).start();
                    } else {
                        Toast.makeText(WifiConnectivityActivity.this, "wrong password", Toast.LENGTH_LONG).show();
                    }
                } else if (connectivityState == 2) {
                    if (deviceNetId == wifiManager.getConnectionInfo().getNetworkId()) {
                        new Thread(new ClientThread()).start();
                    } else {
                        Toast.makeText(WifiConnectivityActivity.this, "unable to connect to device", Toast.LENGTH_LONG).show();
                        ConnectBack();
                    }
                }
            }
            int supl_error = intent.getIntExtra(
                    WifiManager.EXTRA_SUPPLICANT_ERROR, -1);
            //WifiManager.ERROR_AUTHENTICATING
            System.out.println("action recieved is supplicant " + supl_state );
            if (supl_error == WifiManager.ERROR_AUTHENTICATING) {
                Toast.makeText(WifiConnectivityActivity.this, "wrong password", Toast.LENGTH_LONG).show();
                ConnectBack();
                System.out.println("action recieved is error " + supl_error );
            }

            if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION )) {
                isConnectedViaWifi();
                if (intent.getBooleanExtra(EXTRA_SUPPLICANT_CONNECTED, false)){
                    //do stuff
                    isConnectedViaWifi();
                } else {
                    // wifi connection was lost
                }
            }
        }
    };

    private boolean isConnectedViaWifi() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        System.out.println("wifi connected = " + mWifi.isConnected());
        return mWifi.isConnected();
    }

    public static String getCurrentSsid(Context context) {
        String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
                oldNetId = connectionInfo.getNetworkId();
            }
        }
        return ssid;
    }


    public  int getCurrentNetId(Context context) {
        String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
                oldNetId = connectionInfo.getNetworkId();
            }
        }
        return oldNetId;
    }

    class Recieve implements Runnable {

        @Override
        public void run() {
            InputStream in = null;
            try {
                in = socket.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String test = br.toString();
                String line;
                int bytesRead = -1;
                ByteArrayOutputStream byteArrayOutputStream =
                        new ByteArrayOutputStream(1024);
                byte[] buffer = new byte[1024];
                String response = "";
                int state = 1;
                while ((bytesRead = in.read(buffer)) != -1){
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                    response = byteArrayOutputStream.toString("UTF-8");
                    System.out.println("recieved " + response);
                    socketResponse = response;
                    WifiConnectivityActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(WifiConnectivityActivity.this, socketResponse , Toast.LENGTH_LONG).show();
                        }//public void run() {
                    });
                    Gson gson = new Gson();
                    TCPScoketResponse resp = gson.fromJson(response, TCPScoketResponse.class);
                    if (state == 1) {
                        if (resp.getRes().equals(recievedMac)) {
                            state = 2;
                            buffer = new byte[1024];
                            byteArrayOutputStream =
                                    new ByteArrayOutputStream(1024);
                            new Thread(new HandleMsg()).start();
                        } else {

                            ConnectBack();
                        }
                    } else {

                        if (resp.getRes().equals("VALIDATION_SUCCESSFUL")) {
                            // Toast.makeText(WifiConnectivityActivity.this, "device connected to network", Toast.LENGTH_LONG).show();
                        } else {
                            // Toast.makeText(WifiConnectivityActivity.this, "wrong password on device", Toast.LENGTH_LONG).show();
                        }
                        success = 1;
                        ConnectBack();
                    }



                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    class ValidateWifi implements Runnable {

        @Override
        public void run() {
            Thread timer=new Thread()
            {
                public void run()
                {
                    try
                    {
                        sleep(10000);




                    }catch(Exception e){
                        e.printStackTrace();
                    }finally
                    {
                        String ssid = getCurrentSsid(WifiConnectivityActivity.this);
                        if (ssid.equals("\"" + ssidForDevice + "\"")) {
                            String deviceSSID = GetDeviceSSID();
                            if (deviceSSID != null) {

                                ConnectToDevice(deviceSSID);

                            } else {
                                WifiConnectivityActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(WifiConnectivityActivity.this, "No device available", Toast.LENGTH_LONG).show();
                                    }//public void run() {
                                });

                                ConnectBack();
                            }
                        }

                    }

                }

            };
            timer.start();



        }
    }


    void SendMessage(String text)
    {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream())),true);
            out.println(text);
            //       socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    class ClientThread implements Runnable {

        @Override
        public void run() {

            try {
                InetAddress serverAddr = InetAddress.getByName("192.168.10.1");

                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                socket = new Socket(serverAddr, 50007);
                WifiConnectivityActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(WifiConnectivityActivity.this, "socket connected" , Toast.LENGTH_LONG).show();
                    }//public void run() {
                });

                new Thread(new Recieve()).start();
                Thread timer=new Thread()
                {
                    public void run()
                    {
                        try
                        {
                            sleep(3000);




                        }catch(Exception e){
                            e.printStackTrace();
                        }finally
                        {
                            SendMessage("{\"req\":\"MAC\"}");

                        }

                    }

                };
                timer.start();



            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }

    }


    class HandleMsg implements Runnable {

        @Override
        public void run() {

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            SendMessage("{\"req\":\"VALIDATE_WIFI\", \"ssid\":\"" + ssidForDevice + "\", \"password\":\"" + password + "\"}") ;

        }
    }



    class AddDeviceToServer extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";
        int statusCode = 0;
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {

            progress = new ProgressDialog(WifiConnectivityActivity.this);
            progress.setMessage("Saving device ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();



            try{
                post = new HttpPost(CommonConstants.SERVER_URL + "/v1/device/" + did);
                json.put("user_id", uid);
                json.put("device_id",did );
                json.put("name",name );
                json.put("device_type", "BULB");
                json.put("configured", true);

                ByteArrayEntity se = new ByteArrayEntity(json.toString().getBytes("UTF8"));
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);
				/*Checking response */
				/*if(response!=null){
	                InputStream in = response.getEntity().getContent(); //Get the data in the entity
				 */
                String responseString;
                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(response.getEntity());
                Log.i("tag", responseString);


                System.out.println("sign up string " + responseString);

                data = responseString;
                System.out.println("returned " + data);

                return data;




            }
            catch(Exception e){
                e.printStackTrace();
                // createDialog("Error", "Cannot Estabilish Connection");
            }


            return null;
        }

        protected void onPostExecute(String result) {


            if (progress != null) {
                progress.dismiss();
            }
            if (statusCode == 201 || statusCode == 200) {

                Gson gson = new Gson();
                User user = gson.fromJson(data, User.class);

                    AddDevice();
                    Intent intent = new Intent(WifiConnectivityActivity.this, DeviceLandingActivity.class);
                    startActivity(intent);


            }

            System.out.println("sign up string " + data);
            Gson gson = new Gson();

            //    }

            //    else {

            //    }

        }

    }

}
