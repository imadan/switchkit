package com.optime.switchkit.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.optime.switchkit.Adapter.DeviceListAdapter;
import com.optime.switchkit.DB.DeviceDBHelper;
import com.optime.switchkit.DB.DeviceDBUtility;
import com.optime.switchkit.Model.Device;
import com.optime.switchkit.Model.GetDeviceResponse;
import com.optime.switchkit.Model.MacResponse;
import com.optime.switchkit.Model.TCPScoketResponse;
import com.optime.switchkit.R;
import com.optime.switchkit.Utility.CommonConstants;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by rmadan on 7/4/2015.
 */
public class DeviceLandingActivity extends Activity {
    ListView deviceList;
    ImageView addButton;
    ArrayList<Device> devices;

    DeviceListAdapter deviceListAdapter;
    DeviceDBHelper deviceDBHelper;
    DeviceDBUtility deviceDBUtility;

    int addState = 0;
    String recievedMac = "";
    String uid;


    Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_screen);

        InitializeViews();
        InitializeListeners();
        InitializeDB();
        GetValuesFromPreferences();
        if (getIntent().getIntExtra("just_logged", 0) == 1) {
        new GetDevices().execute();
        }

    }

    void InitializeViews()
    {
        deviceList = (ListView)findViewById(R.id.device_list);
        addButton = (ImageView)findViewById(R.id.add_view);
    }

    void InitializeListeners()
    {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowUidPopUp();
                // recievedMac = "C8:93:46:41:D6:39";
                // Intent intent = new Intent(DeviceLandingActivity.this, WifiConnectivityActivity.class);
                // intent.putExtra("mac", recievedMac);
                // startActivity(intent);

          /*      Device device = new Device();
                if (devices == null || devices.size() == 0) {
                    device.setDid("1");
                } else {
                    device.setDid(devices.get(devices.size() - 1).getDid() + 1);
                }
                device.setName("Bedroom");
                device.setState(true);
                device.setBrightness(60);
                device.setColor(0x880000);
                device.setSharedNames("Rohit,Rakesh");
                device.setWifiPass("12345");
                device.setWifiSSID("rohit");
                devices.add(device);

                PopulateViews();
                AddDeviceToDB(device);
*/              //  SendMessage("{\"req\":\"C8:93:46:41:D6:39\"}");
//                SendMessage("{\"req\":\"MAC\"}");
                //   SendMessage("{\"req\":\"VALIDATE_WIFI\",\"ssid\":\"optime_airtel\", \"password\":\"optime_123\"}");
                // SendMessage("{\"req\":\"WIFI_LIST\"}");
                //         CreateSocket();

            }
        });

        deviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(DeviceLandingActivity.this, DeviceSettingActivity.class);
                intent.putExtra("did", devices.get(position).getDevice_id());
                startActivity(intent);
            }
        });
    }


    void PopulateViews()
    {
        if (devices != null && devices.size()>0) {
//            if (deviceListAdapter == null) {
            deviceListAdapter = new DeviceListAdapter(DeviceLandingActivity.this, devices);
            deviceList.setAdapter(deviceListAdapter);
//            } else {
//                deviceListAdapter.notifyDataSetChanged();
//            }
        }

    }

    void GetValuesFromDB()
    {
        devices = deviceDBUtility.GetDevice(deviceDBHelper, "");
    }

    void InitializeDB()
    {
        deviceDBUtility = new DeviceDBUtility();
        deviceDBHelper = new DeviceDBHelper(DeviceLandingActivity.this);



    }

    void AddDeviceToDB(Device device)
    {
        deviceDBUtility.AddStatusToDB(deviceDBHelper, device);

    }

    void GetValuesFromPreferences()
    {
        SharedPreferences userSettings;
        userSettings = DeviceLandingActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        uid = userSettings.getString(CommonConstants.USER_ID, "");

    }

    @Override
    protected void onResume() {
        super.onResume();
        GetValuesFromDB();
        PopulateViews();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.device_list_menu, menu);

        // Associate searchable configuration with the SearchView

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        Context context = DeviceLandingActivity.this;
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(DeviceLandingActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.action_share:
                Intent shareIntent = new Intent(DeviceLandingActivity.this, ShareActivity.class);
                startActivity(shareIntent);
                break;
        }
        return false;
    }


    void CreateSocket()
    {
        try {
            InetAddress serverAddr = InetAddress.getByName("192.168.0.104");
            //socket = new Socket(serverAddr, 3339);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void ShowUidPopUp()
    {
        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DeviceLandingActivity.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Add UID");

        // Setting Dialog Message
        //alertDialog.setMessage("");
        final EditText input = new EditText(DeviceLandingActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        alertDialog.setView(input);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        String inputText = input.getText().toString();
                        if (inputText.length() == 4) {
                         new GetSharedDevices(inputText).execute();
                        } else {
                            new GetdeviceMac(inputText).execute();
                            Intent intent = new Intent(DeviceLandingActivity.this, WifiConnectivityActivity.class);
                            intent.putExtra("mac", recievedMac);
                            intent.putExtra("did", "123");
                        }
                        //      startActivity(intent);

                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
    }

    void SendMessage(String text)
    {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream())),true);
            out.println(text);
            //       socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    class Recieve implements Runnable {

        @Override
        public void run() {
            InputStream in = null;
            try {
                in = socket.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String test = br.toString();
                String line;
                int bytesRead = -1;
                ByteArrayOutputStream byteArrayOutputStream =
                        new ByteArrayOutputStream(1024);
                byte[] buffer = new byte[1024];
                String response = "";
                while ((bytesRead = in.read(buffer)) != -1){
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                    response = byteArrayOutputStream.toString("UTF-8");

                    Gson gson = new Gson();
                    TCPScoketResponse resp = gson.fromJson(response, TCPScoketResponse.class);
                    if (resp.getRes().equals(recievedMac)) {
                        new Thread(new HandleMsg()).start();
                    }
                    System.out.println("recieved " + response);
                    addState = 2;

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class HandleMsg implements Runnable {

        @Override
        public void run() {

            //  if (addState == 1) {

            Intent intent = new Intent(DeviceLandingActivity.this, WifiConnectivityActivity.class);
            startActivity(intent);
            //  }

        }
    }

    class ClientThread implements Runnable {

        @Override
        public void run() {

            try {
                InetAddress serverAddr = InetAddress.getByName("192.168.10.1");

                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                socket = new Socket(serverAddr, 50007);
                new Thread(new Recieve()).start();
                Thread timer=new Thread()
                {
                    public void run()
                    {
                        try
                        {
                            sleep(3000);




                        }catch(Exception e){
                            e.printStackTrace();
                        }finally
                        {
                            SendMessage("{\"req\":\"MAC\"}");

                        }

                    }

                };
                timer.start();



            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }

    }



    class GetdeviceMac  extends AsyncTask<String, String, String> {

        String data;
        String did;

        GetdeviceMac(String did)
        {
            this.did = did;
        }
        @Override
        protected String doInBackground(String... uri) {
            BufferedReader in = null;


            try
            {

                HttpClient client = new DefaultHttpClient();
                //URI website = new URI("https://api.instagram.com/v1/users/self/feed?access_token=470510647.cff9b6b.70cb3100f27b4fd8be33864e54fb0a1c" );
                URI website = new URI(CommonConstants.SERVER_URL + "/v1/device/" + did );
                System.out.println("url hit is " + website.toString());
                HttpGet request = new HttpGet();
                request.setURI(website);
                HttpResponse response = client.execute(request);
                response.getStatusLine().getStatusCode();

                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                StringBuffer sb = new StringBuffer("");
                String l = "";
                String nl = System.getProperty("line.separator");
                while ((l = in.readLine()) !=null){
                    sb.append(l + nl);
                }


                in.close();
                data = sb.toString();
                System.out.println("returned " + data);

                return data;
            }
            catch (Exception e){
                e.printStackTrace();
            } finally {

                if (in != null){
                    try{
                        in.close();
                        return data;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

            }

            return null;
        }

        protected void onPostExecute(String result) {


            if (data != null) {

                //data = "{\"response\":" + data + "}";
                System.out.println("sign up string " + data);
                Gson gson = new Gson();
                MacResponse resp = gson.fromJson(data, MacResponse.class);
                //recievedMac = "C8:93:46:41:D6:39";
                if (!resp.isConfigured()) {


                    recievedMac = resp.getMac();
                    Intent intent = new Intent(DeviceLandingActivity.this, WifiConnectivityActivity.class);
                    intent.putExtra("mac", recievedMac);
                    intent.putExtra("did", did);
                    startActivity(intent);
                } else {
                    Toast.makeText(DeviceLandingActivity.this, "Device already configured", Toast.LENGTH_LONG).show();
                }
                // ConnectToDevice("SWITCH_C8:93:46:41:D6:39_NODE");

                // new Thread(new ClientThread()).start();
            }
        }


    }



    class GetDevices  extends AsyncTask<String, String, String> {

        String data;
        String did;


        @Override
        protected String doInBackground(String... uri) {
            BufferedReader in = null;


            try
            {

                HttpClient client = new DefaultHttpClient();
                //URI website = new URI("https://api.instagram.com/v1/users/self/feed?access_token=470510647.cff9b6b.70cb3100f27b4fd8be33864e54fb0a1c" );
                URI website = new URI(CommonConstants.SERVER_URL + "/v1/user/" + uid + "/devices" );
                System.out.println("url hit is " + website.toString());
                HttpGet request = new HttpGet();
                request.setURI(website);
                HttpResponse response = client.execute(request);
                response.getStatusLine().getStatusCode();

                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                StringBuffer sb = new StringBuffer("");
                String l = "";
                String nl = System.getProperty("line.separator");
                while ((l = in.readLine()) !=null){
                    sb.append(l + nl);
                }


                in.close();
                data = sb.toString();
                System.out.println("returned " + data);

                return data;
            }
            catch (Exception e){
                e.printStackTrace();
            } finally {

                if (in != null){
                    try{
                        in.close();
                        return data;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

            }

            return null;
        }

        protected void onPostExecute(String result) {

            if (data != null) {

                data = "{\"devices\":" + data + "}";
                System.out.println("sign up string " + data);
                Gson gson = new Gson();
                GetDeviceResponse resp = gson.fromJson(data, GetDeviceResponse.class);

                devices = resp.getDevices();
                if (devices != null) {
                    PopulateViews();
                    for (int i=0; i<devices.size(); i++) {
                        AddDeviceToDB(devices.get(i));
                    }
                }

                // new Thread(new ClientThread()).start();
            }
        }


    }

    public void ConnectToRouter(String ssid, String pwd)
    {

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", ssid);
        wifiConfig.preSharedKey = String.format("\"%s\"", pwd);

        WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
//remember id
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();
    }


    public void ConnectToDevice(String ssid)
    {

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", ssid);
        wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
//remember id
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();
    }


    class GetSharedDevices extends AsyncTask<String, String, String> {

        String data;
        String url = "initial";
        int statusCode = 0;
        ProgressDialog progress;
        String otp;

        GetSharedDevices(String otp)
        {
            this .otp = otp;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(DeviceLandingActivity.this);
            progress.setMessage("Logging in :) ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpPost post = null;
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
            HttpResponse response;
            JSONObject json = new JSONObject();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();



            try{
                post = new HttpPost(CommonConstants.SERVER_URL + "/v1/user/otp");
                json.put("otp", Integer.valueOf(otp));
                json.put("user_id", uid);

                ByteArrayEntity se = new ByteArrayEntity(json.toString().getBytes("UTF8"));
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);
				/*Checking response */
				/*if(response!=null){
	                InputStream in = response.getEntity().getContent(); //Get the data in the entity
				 */
                String responseString;
                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(response.getEntity());
                Log.i("tag", responseString);


                System.out.println("sign up string " + responseString);

                data = responseString;
                System.out.println("returned " + data);

                return data;

            }
            catch(Exception e){
                e.printStackTrace();
                // createDialog("Error", "Cannot Estabilish Connection");
            }
            return null;
        }

        protected void onPostExecute(String result) {
            if (progress != null) {
                progress.dismiss();
            }
            if (statusCode == 201 || statusCode == 200) {
                data = "{\"devices\":" + data + "}";

                System.out.println("sign up string " + data);
                Gson gson = new Gson();
                GetDeviceResponse resp = gson.fromJson(data, GetDeviceResponse.class);

                ArrayList<Device> newDevices = resp.getDevices();
                if (newDevices != null) {

                    for (int i=0; i<newDevices.size(); i++) {
                        if (devices != null) {
                            devices.add(newDevices.get(i));
                        } else {
                            devices = newDevices;
                        }
                        AddDeviceToDB(newDevices.get(i));
                    }
                    PopulateViews();
                }



            }


            //    }

            //    else {

            //    }

        }

    }


}